#!/usr/bin/env bash

################
#  CONSTANTES  #
################

CREER_PROJ_SCRIPT="./creer_projet" # Chemin vers le script à tester

TEMPLATE_="template"               # Répertoire contenant des fichiers avec lesquels créer le
                                   # dépôt (option -t)

DEFAULT_="default"                 # Répertoire contenant les fichiers par défaut avec lesquels
                                   # créer le dépôt

NOM_PROJET_="titi"                 # Le nom du projet à créer

# modificateurs de couleurs
RED="\033[0;31m"
GREEN="\033[0;32m"
NO_COLOUR="\033[0m"
BOLD="\e[1m"

###################
#  SOURCE SCRIPT  #
###################

# On source le script afin d'obtenir les primitives nécessaires. Par exemple, on
# pourra appeler la fonction `print_help` depuis le script
# `${CREER_PROJ_SCRIPT}`.
source $CREER_PROJ_SCRIPT

###########
#  TESTS  #
###########

# Fonction de test de la commande `${CREER_PROJ_SCRIPT}`
function test_arg_empty() {
  # Exécute la commande et fait la différence entre la sortie de celle-ci:
  #   <($CREER_PROJ_SCRIPT)
  # et la sortie attendue écrite à la mitaine:
  #   <(printf "Erreur: Le nom du projet doit être spécifié.\n"; print_help)
  #
  # Le résultat du `diff` est ensuite retourné comme la sortie standard de la
  # fonction `test_arg_empty`. On pourra alors récupérer la sortie en faisant:
  # `la_sortie=$(test_arg_empty)` (voir l'appel à la fin du script).
  #
  # REMARQUE 1: L'écriture <(commande) est substitué par le nom du fichier
  # contenant la sortie standard de la commande <commande>.
  #
  # REMARQUE 2: Puisqu'il s'agit d'une seule commande écrite sur plusieurs
  # ligne, on doit ajouter des '\' à la fin de chaque ligne.
  #
  # REMARQUE 3: On utilise 2>&1 pour rediriger (>) la sortie d'erreur (2) dans
  # la sortie standard (1).
  diff <($CREER_PROJ_SCRIPT) \
       <(printf "Erreur: Le nom du projet doit être spécifié.\n"; print_help) \
       2>&1
}

# Fonction de test de la commande `${CREER_PROJ_SCRIPT} ${NOM_PROJET_}`
#   ${1}: le nom du projet (le répertoire qui sera créé)
#   ${2}: le template (d'où prendre les fichiers à copier)
function test_created_files() {
  # On exécute premièrement le script. Puisqu'on ne veut pas la sortie d'erreur
  # (2) ni la sortie standard (1), on la redirige dans les limbes: /dev/null.
  #
  # Si on ${2} est non-vide, alors c'est la forme
  #   `<prog> -t <template> <nom>`.
  # Sinon, c'est la forme
  #   `<prog> <nom>`
  nom_projet_=${1}
  template_=${2}
  if [[ "${template_}" != "" ]]; then
    $CREER_PROJ_SCRIPT -t ${template_} ${nom_projet_} 1>/dev/null 2>/dev/null
  else
    template_=${DEFAULT_}
    $CREER_PROJ_SCRIPT ${nom_projet_} 1>/dev/null 2>/dev/null
  fi

  # Un peu comme la fonction plus haut, on compare la sortie de deux commandes.
  # Ici, c'est la liste des fichiers qui se trouve dans chaque répertoire. Cette
  # liste est donnée par `find`. On décortique la commande find ci-après:
  #
  #   - `-type f`             Filtre les lignes pour n'avoir que des fichiers;
  #   - `! -path "*.git/*"`   Filtre que les chemins qui ne contiennent pas le
  #                           répertoire ".git/". On ne veut pas comparer ses
  #                           fichiers car ils seront forcément différents à
  #                           chaque test (les fichiers git dépendent de l'heure
  #                           entre autres).
  #   - `-exec sha1sum {} \;` Pour chaque fichier que find trouve, il exécute la
  #                           commande `sha1sum` avec premier argument, le nom
  #                           du fichier `{}`. On termine la commande par `\;`
  #                           pour éviter l'analyse du shell (voir `man 1 find`)
  #
  # La sortie de `find` est ensuite filtrée avec `sed` afin de se débarasser des
  # termes qu'on ne souhaite pas apparaître. Puisque les fichier sont pris dans
  # deux répertoires différents, on ne veut pas différencier `default/Makefile`
  # de `titi/Makefile`. Ces deux fichiers ne doivent pas créer de différence. On
  # retire donc de toutes les lignes les répertoire parent. Dans l'exemple
  # précédent, c'est `default` (${1}) et `titi` (${2}). Finalement, on trie en
  # ordre lexicographique la sortie finale.
  #
  # À la fin, si tous les fichiers sont présents dans les deux répertoire, la
  # sortie des deux commandes "find" devraient être les mêmes. Donc,  ̀diff`
  # devrait réussir.
  #
  # REMARQUE: L'utilisation de `sha1sum` permet non seulement de vérifier le nom
  #           du fichier, mais l'intégrité du fichier, c.-à-d. qu'on vérifie
  #           aussi le contenu (voir
  #           https://fr.wikipedia.org/wiki/Fonction_de_hachage_cryptographique)
  diff <(find ${nom_projet_} -type f ! -path "*.git/*" -exec sha1sum {} \; \
         | sed -e "s,${nom_projet_}/,,g" \
               -e "s,.gitignore,gitignore,g" \
         | sort) \
       <(find ${template_} -type f ! -path "*.git/*" -exec sha1sum {} \; \
         | sed "s,${template_}/,,g" \
         | sort) \
       2>&1
}


#################
#  UTILITAIRES  #
#################

# Nettoyage des tests
function clean_projet() { rm -rf ${NOM_PROJET_}; }

# Écrire le test à la sortie standard
#   ${@}: Tous les paramètres passés, c.-à-d. la liste $1 $2 $3 ...
function print_test() { printf "${BOLD}Test de \'${1}\':${NO_COLOUR} "; }

# Écrire le message de succès
function print_success() { printf "${GREEN}Succès${NO_COLOUR}!\n"; }

# Écrire le message d'échec. Celui-ci est composé du mot "Échec", suivi du texte
# additionnel passé en paramètre ($1).
#   ${1}: le diff entre la sortie attendue et la sortie reçue.
function print_failure() { printf "${RED}Échec${NO_COLOUR}.\n${1}\n"; }

#########################
#  Exécution des tests  #
#########################

# Appel de la fonction print_test. La variable ${CREER_PROJ_SCRIPT} est sont
# premier argument (${1}).
print_test ${CREER_PROJ_SCRIPT}

# On appelle `test_arg_empty`. L'expression après && est exécuté seulement si le
# dernier programme a retourné un code de sortie == 0. Sinon, c'est l'expression
# après || qui est exécuté.
#
# La sortie de la fonction `test_arg_empty` est récupérée dans la variable
# `diff`.
diff="$(test_arg_empty)" && print_success || print_failure "${diff}"

# Appel de la fonction print_test. L'expression entre guillemets
# "${CREER_PROJ_SCRIPT} ${NOM_PROJET_}" est le premier argument (${1}).
print_test "${CREER_PROJ_SCRIPT} ${NOM_PROJET_}"

diff=$(test_created_files ${NOM_PROJET_}) && print_success || print_failure "${diff}"
clean_projet

print_test "${CREER_PROJ_SCRIPT} -t ${TEMPLATE_} ${NOM_PROJET_}"
diff=$(test_created_files ${NOM_PROJET_} ${TEMPLATE_}) && print_success || print_failure "${diff}"
clean_projet

# # vim:set et sts=2 sw=2 ts=2 tw=80:

