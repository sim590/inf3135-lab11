# inf3135-lab11

Il s'agit des solutions pour le laboratoire 11 du cours INF3135 à l'automne 2017
à l'Université du Québec à Montréal.

## État des solutions

### Complétés

Numéros 1,2 et 3.

### Manquants

Numéro 4.

## Dépendances

Les programmes suivants sont nécessaires à l'exécution:

- `readlink`
- `getopt`
- `git`

L'implémentation n'est pa entièrement POSIX (faute de temps). Il est donc prévu
pour rouler sur GNU/Linux et pas d'autre plateforme. Par exemple,
l'implémentation d'un ou plusieurs de ces programmes peut différer dépendamment
de la plateforme:

- `readlink`
- `getopt`

